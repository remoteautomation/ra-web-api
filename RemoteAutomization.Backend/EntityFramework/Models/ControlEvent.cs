using System;

namespace RemoteAutomization.Backend.EntityFramework.Models
{
    /// <summary>
    /// Event representing a state change
    /// </summary>
    public class ControlEvent
    {
        /// <summary>
        /// Event identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Control identifier
        /// </summary>
        public uint ControlId { get; set; }
        /// <summary>
        /// Value of the current event
        /// </summary>
        public ushort Value { get; set; }
        /// <summary>
        /// Changer/triggering entity
        /// </summary>
        /// <value></value>
        public uint Changer { get; set; }
        /// <summary>
        /// Timestamp of the event
        /// </summary>
        public DateTime TimeStamp { get; set; }
    }
}