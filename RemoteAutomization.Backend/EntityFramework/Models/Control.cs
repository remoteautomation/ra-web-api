using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RemoteAutomization.Backend.EntityFramework.Models
{
    /// <summary>
    /// EF Control model
    /// </summary>
    public class Control : IoTControl
    {
        /// <summary>
        /// Current controlservice identifier
        /// </summary>
        /// <value></value>
        public uint ControlServiceId { get; set; }
        /// <summary>
        /// Name of the IoT control
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Control type of the current IoTControl
        /// </summary>
        [EnumDataType(typeof(ControlType))]
        public ControlType Type { get; set; }
        /// <summary>
        /// Invert command  (DIGITAL controls only).
        /// </summary>
        public bool Inverted { get; set; }
        /// <summary>
        /// Last control changer
        /// </summary>
        /// <value></value>
        public uint LastChanger { get; set; }
        /// <summary>
        /// Control model modified timestamp
        /// </summary>
        /// <value></value>
        public DateTime Modified { get; set; }
        /// <summary>
        /// Control model modifier
        /// </summary>
        /// <value></value>
        public uint ModifiedBy { get; set; }

        /// <summary>
        /// The corresponding events of the control
        /// </summary>
        public List<ControlEvent> Events { get; set; }
            = new List<ControlEvent>();

        /// <summary>
        /// Convert the inserted value into the digitally correct value taking the Inverted setting into consideration
        /// </summary>
        /// <param name="value">The value to (possibly) change</param>
        /// <returns>the correct digital value</returns>
        public ushort GetDigitalValue(ushort value)
        {
            if (Inverted)
                switch (value)
                {
                    case 0:
                        value = 1;
                        break;
                    default:
                    case 1:
                        value = 0;
                        break;
                }
            
            return value;
        }
    }

    /// <summary>
    /// Control type enum for easy development
    /// </summary>
    public enum ControlType : byte
    {
        DIGITAL,
        ANALOG
    }
}