using System;

namespace RemoteAutomization.Backend.EntityFramework.Models
{
    public class UserAccount
    {
        public uint Id { get; set; }
        public string Username { get; set; }
        public string NormalizedUsername { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public uint ModifiedBy { get; set; }
        public bool Enabled { get; set; }
    }

    public class UserRoles
    {
        public const string Owner = "owner";
        public const string Admin = "admin";
        public const string User = "user";
    }
}