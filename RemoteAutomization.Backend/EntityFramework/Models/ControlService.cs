using System;
using System.Collections.Generic;

namespace RemoteAutomization.Backend.EntityFramework.Models
{
    /// <summary>
    /// Endpoint class for IoT services (Mostly Arduinos)
    /// </summary>
    public class ControlService
    {
        /// <summary>
        /// Service identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Name of the service
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Current online status of the service
        /// </summary>
        public bool Connected { get; set; }
        /// <summary>
        /// Service's authentication token
        /// </summary>
        public string AuthToken { get; set; }
        /// <summary>
        /// Last seen timestamp
        /// </summary>
        public DateTime LastSeen { get; set; }
        /// <summary>
        /// Last modified timestamp
        /// </summary>
        public DateTime Modified { get; set; }
        /// <summary>
        /// Last modified entity
        /// </summary>
        public uint ModifiedBy { get; set; }
        /// <summary>
        /// Child controls
        /// </summary>
        public List<Control> Controls { get; set; }
            = new List<Control>();
    }
}