using System;
using Microsoft.EntityFrameworkCore;

using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend.EntityFramework
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<UserAccount> Users { get; set; }
        public DbSet<ControlService> ControlServices { get; set; }
        public DbSet<Control> Controls { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ControlService>()
                .Property(x => x.Name)
                    .HasColumnType("varchar(64)")
                    .HasMaxLength(64)
                    .IsRequired();
            modelBuilder.Entity<ControlService>()
                .Property(x => x.AuthToken)
                    .HasColumnType("varchar(60)")
                    .HasMaxLength(60)
                    .IsRequired();
            modelBuilder.Entity<ControlService>()
                .Property(x => x.Modified)
                    .HasConversion(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
            modelBuilder.Entity<ControlService>()
                .Property(x => x.LastSeen)
                    .HasConversion(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
            
            modelBuilder.Entity<Control>()
                .Property(x => x.Name)
                    .HasColumnType("varchar(64)")
                    .HasMaxLength(64)
                    .IsRequired();
            modelBuilder.Entity<Control>()
                .Property(x => x.LastChanged)
                    .HasConversion(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
            modelBuilder.Entity<Control>()
                .Property(x => x.Modified)
                    .HasConversion(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            modelBuilder.Entity<UserAccount>()
                .Property(x => x.FirstName)
                    .HasColumnType("varchar(32)")
                    .HasMaxLength(32)
                    .IsRequired();
            modelBuilder.Entity<UserAccount>()
                .Property(x => x.LastName)
                    .HasColumnType("varchar(32)")
                    .HasMaxLength(32)
                    .IsRequired();
            modelBuilder.Entity<UserAccount>()
                .Property(x => x.Username)
                    .HasColumnType("varchar(24)")
                    .HasMaxLength(24)
                    .IsRequired();
            modelBuilder.Entity<UserAccount>()
                .Property(x => x.NormalizedUsername)
                    .HasColumnType("varchar(24)")
                    .HasMaxLength(24)
                    .IsRequired();
            modelBuilder.Entity<UserAccount>()
                .Property(x => x.Password)
                    .HasColumnType("varchar(60)")
                    .HasMaxLength(60)
                    .IsRequired();
            modelBuilder.Entity<UserAccount>()
                .Property(x => x.Modified)
                    .HasConversion(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
        }
    }
}