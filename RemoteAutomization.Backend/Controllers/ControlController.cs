using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend.Controllers
{
    /// <summary>
    /// The controller endpoint
    /// </summary>
    [ApiController, Authorize]
    public class ControlController : ControllerBase
    {
        private readonly ILogger _log;
        private readonly AppDbContext _dbContext;
        public ControlController(ILogger<IoTWebSocketController> logger, AppDbContext dbContext)
        {
            _log = logger;
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get the control
        /// </summary>
        /// <param name="id">Control identifier</param>
        /// <returns>The requested Control entity</returns>
        [HttpGet("control/{id}")]
        [ProducesResponseType(typeof(Control), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ActionResult<Control>> GetControl(uint id)
        {
            Control control =
                await _dbContext.Controls
                    .Where(c => c.Id == id)
                    .FirstOrDefaultAsync();
            
            if (control == null)
                return NotFound();

            return Ok(control);
        }

        /// <summary>
        /// Retrieve all the events from a specific controller
        /// </summary>
        /// <param name="id">Controller identifier</param>
        /// <returns>An array of ControlEvents</returns>
        [HttpGet("control/{id}/events")]
        [ProducesResponseType(typeof(ControlEvent[]), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ActionResult<ControlEvent[]>> GetEvents(uint id)
        {
            ControlEvent[] events = 
                await _dbContext.Controls
                    .Include(c => c.Events)
                    .Where(c => c.Id == id)
                    .Select(c => c.Events.ToArray()).FirstOrDefaultAsync();

            if (events == null || events.LongLength <= 0)
                return NotFound();

            return Ok(events);
        }

        /// <summary>
        /// Set the controller value
        /// </summary>
        /// <param name="control">The IoTControl entity with the id and the new value</param>
        /// <returns>Void</returns>
        [HttpPost("control/value")]
        [ProducesResponseType(typeof(OkResult), 200)]
        [ProducesResponseType(typeof(APIErrorResponse), 400)]
        public async Task<ActionResult> SetControlValue(IoTControl control)
        {
            if (control.Id <= 0)
                return BadRequest( new APIErrorResponse("CONTROL_INVALID", "The specified control identifier was not valid.") );
            
            Control ctrl = await _dbContext.Controls
                                    .Where(c => c.Id == control.Id)
                                    .Select(c => new Control() { Id = c.Id, ControlServiceId = c.ControlServiceId, Value = c.Value })
                                    .FirstOrDefaultAsync();
            if (ctrl == null)
                return BadRequest( new APIErrorResponse("CONTROL_NOT_FOUND", "The specified control identifier does not exist.") );

            if (ctrl.Value != control.Value)
            {
                ctrl.Events.Add(
                    new ControlEvent() {
                        Changer = 1,
                        TimeStamp = DateTime.UtcNow,
                        Value = control.Value
                    });
                if (IoTWebSocketController.Sockets.ContainsKey(ctrl.ControlServiceId))
                {
                    await IoTWebSocketController.Sockets[ctrl.ControlServiceId].SendResponseAsync(
                        new IoTResponse() {
                            Type = IoTResponseType.Command,
                            Controls = new List<IoTControl>() { control }
                        });
                }
                
                
                _dbContext.Attach(ctrl);
                await _dbContext.SaveChangesAsync();
            }

            return Ok();
        }
    }
}