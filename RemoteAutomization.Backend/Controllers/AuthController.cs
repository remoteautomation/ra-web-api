using System;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend.Controllers
{
    /// <summary>
    /// Authentication API endpoint
    /// </summary>
    [Route("auth")]
    [ApiController, Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Authentication endpoint
        /// </summary>
        /// <param name="userService">(Dependency Injection) UserService repository</param>
        public AuthController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Login and retrieve your token
        /// </summary>
        /// <param name="request">AuthRequest containing login credentials</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(AuthResponse), 200)]
        [ProducesResponseType(typeof(APIErrorResponse), 401)]
        [ProducesResponseType(typeof(APIErrorResponse), 400)]
        public async Task<ActionResult<AuthResponse>> Login([FromBody]AuthRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Username))
                return BadRequest(new APIErrorResponse(APIResponseStrings.InvalidUsername));
            
            if (string.IsNullOrWhiteSpace(request.Password))
                return BadRequest(new APIErrorResponse(APIResponseStrings.InvalidPassword));

            UserAccount acc = await _userService.Authenticate(request.Username, request.Password);
            if (acc == null || !acc.Enabled)
                return Unauthorized(new APIErrorResponse(APIResponseStrings.AuthFailed));
            
            return Ok(new AuthResponse {
                    Token = _userService.GenerateToken(acc) });
        }

        /// <summary>
        /// Change the current password
        /// </summary>
        /// <param name="request">AuthRequest containing the old and new password</param>
        /// <returns>The success state</returns>
        [HttpPost("changePassword")]
        [ProducesResponseType(typeof(OkResult), 200)]
        [ProducesResponseType(typeof(APIErrorResponse), 401)]
        [ProducesResponseType(typeof(APIErrorResponse), 400)]
        [ProducesResponseType(typeof(APIErrorResponse), 500)]
        public async Task<ActionResult> ChangePassword([FromBody]AuthRequest request)
        {            
            if (string.IsNullOrWhiteSpace(request.Password))
                return BadRequest(new APIErrorResponse(APIResponseStrings.InvalidPassword));

            if (string.IsNullOrWhiteSpace(request.OldPassword))
                return BadRequest(new APIErrorResponse(APIResponseStrings.InvalidOldPassword));
            
            UserAccount acc = await _userService.Authenticate(
                                        Convert.ToUInt32(
                                            this.User.Claims
                                                .FirstOrDefault(c => c.Type == ClaimTypes.Sid)
                                                .Value),
                                        request.OldPassword);
            if (acc == null || !acc.Enabled)
                return Unauthorized(new APIErrorResponse(APIResponseStrings.AuthFailed));

            if(!await _userService.ChangePassword(acc.Id, request.Password))
                return StatusCode(500, new APIErrorResponse(APIResponseStrings.InternalError));
            
            return Ok();
        }

        /// <summary>
        /// Verify the current session token.
        /// </summary>
        /// <returns>200 OK if valid</returns>
        [HttpGet("verify")]
        [ProducesResponseType(typeof(OkResult), 200)]
        [ProducesResponseType(typeof(void), 401)]
        public ActionResult Verify()
        {
            return Ok(); // Will return unauthorized through the middleware
        }

        // [HttpPost("register"), AllowAnonymous]
        // public async Task<ActionResult<UserAccount>> Register([FromBody]UserAccount userAccount) // Debugging
        // { 
        //     return Ok(await this._userService.CreateUser(userAccount));
        // }
    }
}