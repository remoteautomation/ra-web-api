using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend.Controllers
{
    [Route("iot")]
    [ApiController]
    public class IoTWebSocketController : ControllerBase
    {
        internal static Dictionary<uint, IoTWebSocketController> Sockets
                        = new Dictionary<uint, IoTWebSocketController>(); 
        private static JsonSerializer _serializer
                        = new JsonSerializer() {
                            NullValueHandling = NullValueHandling.Ignore,
                            ContractResolver = new CamelCasePropertyNamesContractResolver() };

        private WebSocket webSocket;
        private ControlService Service;
        private bool looping = true;


        private readonly ILogger _log;
        private readonly AppDbContext _dbContext;
        private readonly TokenHasher _tokenHasher;
        public IoTWebSocketController(ILogger<IoTWebSocketController> logger, AppDbContext dbContext, TokenHasher tokenHasher)
        {
            _log = logger;
            _dbContext = dbContext;
            _tokenHasher = tokenHasher;
        }

        /// <summary>
        /// Simple endpoint for RA controllers to connect to over and initiate websocket.
        /// </summary>
        /// <returns>A task running the websocket</returns>
        [HttpGet]
        public async Task Get()
        {
            if (!HttpContext.WebSockets.IsWebSocketRequest)
                throw new APIException(400, "Invalid WebSocket request");

            webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();
            await handleIoTConnection();
        }

        

        /// <summary>
        /// Internal communication method to safely disconnect the service's websocket
        /// </summary>
        /// <param name="closeMessage"></param>
        /// <returns>Task</returns>
        internal async Task StopService(string closeMessage = null)
        {
            if (string.IsNullOrWhiteSpace(closeMessage) && webSocket != null && webSocket.State == WebSocketState.Open)
            {
                logInformation(closeMessage);
                await webSocket.CloseOutputAsync(WebSocketCloseStatus.PolicyViolation, closeMessage, HttpContext.RequestAborted);
            }

            try {
                if (Service != null && _dbContext != null)
                {
                    Service.Connected = false;
                    Service.LastSeen = DateTime.UtcNow;
                    _dbContext.Update(Service);
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch (ObjectDisposedException) {}
            looping = false;
            HttpContext.Abort();
        }

        private void logDebug(string messageSuffix, object args = null)
        {
            _log.LogDebug("WebSocket {IP}:{Port} " + messageSuffix, 
                HttpContext.Connection.RemoteIpAddress,
                HttpContext.Connection.RemotePort,
                args);
        }
        private void logInformation(string messageSuffix, object args = null)
        {
            _log.LogInformation("WebSocket {IP}:{Port} " + messageSuffix, 
                HttpContext.Connection.RemoteIpAddress,
                HttpContext.Connection.RemotePort,
                args);
        }
        private void logWarning(string messageSuffix, object args = null)
        {
            _log.LogWarning("WebSocket {IP}:{Port} " + messageSuffix, 
                HttpContext.Connection.RemoteIpAddress,
                HttpContext.Connection.RemotePort,
                args);
        }
        private async Task handleIoTConnection()
        {
            try
            {
                CancellationToken ct = HttpContext.RequestAborted;
                while (looping)
                {
                    if (ct.IsCancellationRequested)
                    {
                        break;
                    }
        
                    IoTRequest request = await receiveAsync<IoTRequest>();
                    if(request == null)
                        break;

                    switch (webSocket.State)
                    {
                        case WebSocketState.Open:  
                            await handleIotRequest(request);
                            break;
                        case WebSocketState.CloseReceived:
                            logInformation("Connection close received from {ServiceId}.", Service.Id);
                            looping = false;
                            break;
                        default:
                            logDebug("Unhandled state: {State}", webSocket.State);
                            break;
                    }
                }

            }
            catch (WebSocketException ex)
            {
                _log.LogError(ex, "Websocket exception occured.");
            }

            await StopService().ConfigureAwait(false);
        }


        private async Task handleIotRequest(IoTRequest request)
        {
            
            if (Service == null)
            {
                // NOT AUTHENTICATED

                if (request.Type != IoTRequestType.Auth)
                {
                    await StopService("Attempted unauthenticated access.").ConfigureAwait(false);
                    return;
                }

                if (string.IsNullOrWhiteSpace(request.Token) || request.Service <= 0)
                {
                    await StopService("Authentication invalidated.").ConfigureAwait(false);
                    return;
                }

                Service = await _dbContext.ControlServices
                    .Where(cs => cs.Id == request.Service && _tokenHasher.Compare(cs.AuthToken, request.Token))
                    .FirstOrDefaultAsync();
                if (Service == null)
                {
                    await StopService("Failed authentication.").ConfigureAwait(false);
                    return;
                }

                if (Sockets.ContainsKey(Service.Id))
                {
                    IoTWebSocketController dupeController = Sockets[Service.Id];
                    logWarning("Second socket (concurrently) connected and authenticated as {Service}!", Service.Id);
                    if (dupeController.webSocket != null && dupeController.webSocket.State == WebSocketState.Open)
                        await dupeController.webSocket.CloseOutputAsync(
                            WebSocketCloseStatus.ProtocolError,
                            "DUPLICATE_SERVICE_CONNECTION",
                            dupeController.HttpContext.RequestAborted);
                    
                    
                }

                // Set this controller as the active controller in Sockets dict.
                Sockets[Service.Id] = this;

                Service.Connected = true;
                Service.LastSeen = DateTime.UtcNow;
                _dbContext.Update(Service);
                await _dbContext.SaveChangesAsync();

                logInformation("Authenticated as {Service}", Service.Id);
                IoTResponse response = new IoTResponse() {
                    Type =  IoTResponseType.AuthResponse,
                    Status = IoTResponseStatus.Success
                };
                await SendResponseAsync(response);

            }

            switch (request.Type)
            {
                case IoTRequestType.Poll:
                    IoTResponse response = new IoTResponse() {
                        Type = IoTResponseType.Command,
                        Controls = await _dbContext.Controls
                            .Where(x => x.ControlServiceId == Service.Id)
                            .Select(x => new IoTControl() { Id = x.Id, Value = x.Value, LastChanged = x.LastChanged })
                            .ToListAsync()
                    };

                    await SendResponseAsync(response);
                    break;
                case IoTRequestType.Command:
                    if (request.Controls == null
                    || request.Controls.Count <= 0) {
                        logDebug("sent empty/invalid command value(s)");
                        break;
                    }
                    
                    logDebug("sent {Count} control commands", request.Controls.Count);
                    Console.WriteLine(JsonConvert.SerializeObject(request));
                    request.Controls.ForEach(x => {
                        var control = _dbContext.Controls.Where(c => c.Id == x.Id).FirstOrDefault();
                        if (control == null)
                            return;
                        
                        if (x.LastChanged > DateTime.UtcNow)
                            x.LastChanged = DateTime.UtcNow; // Make sure we don't have a Mr. Future

                        control.Events.Add(
                            new ControlEvent()
                            {
                                ControlId = control.Id,
                                Changer = 0,
                                TimeStamp = x.LastChanged,
                                Value = x.Value
                            });

                        _log.LogInformation("WebSocket {IP}:{Port} Set control {Id} to value: {State} at {LastChanged}", 
                            HttpContext.Connection.RemoteIpAddress,
                            HttpContext.Connection.RemotePort,
                            x.Id, x.Value.ToString(), x.LastChanged);
                    });

                    await _dbContext.SaveChangesAsync();
                    
                    break;
                default:
                    break;
            }
        }

        internal Task SendResponseAsync(IoTResponse body)
        {
            ArraySegment<byte> segment;
            using(var sw = new StringWriter())
            {
                using(var jsonWriter = new JsonTextWriter(sw))
                    _serializer.Serialize(jsonWriter, body);

                segment = new ArraySegment<byte>(
                    Encoding.UTF8.GetBytes(
                        sw.GetStringBuilder().ToString()));
            }
            return webSocket.SendAsync(segment, WebSocketMessageType.Text, true, HttpContext.RequestAborted);
        }
        private async Task<T> receiveAsync<T>()
        {
            try {
                var buffer = new ArraySegment<byte>(new byte[2048]);
                using (var ms = new MemoryStream())
                {
                    WebSocketReceiveResult result;
                    do
                    {
                        HttpContext.RequestAborted.ThrowIfCancellationRequested();
        
                        result = await webSocket.ReceiveAsync(buffer, HttpContext.RequestAborted);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    }
                    while (!result.EndOfMessage);
        
                    ms.Seek(0, SeekOrigin.Begin);
                    if (result.MessageType != WebSocketMessageType.Text)
                    {
                        return default(T);
                    }

                    using (var rdr = new StreamReader(ms, Encoding.UTF8))
                        using (var jsonReader = new JsonTextReader(rdr))
                        {
                            
                            return _serializer.Deserialize<T>(jsonReader);
                        }
                }
            }
            catch
            {
                return default(T);
            }
        }
    }
}