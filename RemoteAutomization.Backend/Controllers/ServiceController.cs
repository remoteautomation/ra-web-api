using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend.Controllers
{
    [Route("services")]
    [ApiController, Authorize]
    public class ServiceController : ControllerBase
    {
        private readonly ILogger _log;
        private readonly AppDbContext _dbContext;
        public ServiceController(ILogger<ServiceController> logger, AppDbContext dbContext)
        {
            _log = logger;
            _dbContext = dbContext;
        }

        /// <summary>
        /// Retrieve all services
        /// </summary>
        /// <returns>A list of services</returns>
        [HttpGet]
        [ProducesResponseType(typeof(ControlService[]), 200)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<ActionResult<ControlService[]>> GetServices()
        {
            var services = await _dbContext.ControlServices.Select(s =>
                                new ControlService() {
                                    Id = s.Id,
                                    Name = s.Name,
                                    Connected = s.Connected,
                                    LastSeen = s.Connected ? DateTime.UtcNow : s.LastSeen
                                }).ToListAsync();

            if (services == null || services.LongCount() <= 0)
                return NoContent();
            
            return Ok(services);
        }


        /// <summary>
        /// Retrieve all controls from a service
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <returns>A list of controls</returns>
        [HttpGet("service/{serviceId}/controls")]
        [ProducesResponseType(typeof(Control[]), 200)]
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ActionResult<Control[]>> GetControls(uint serviceId)
        {
            Control[] controls =
                await _dbContext.Controls
                    .Where(c => c.ControlServiceId == serviceId)
                    .Select(c => new Control {
                        Id = c.Id,
                        ControlServiceId = serviceId,
                        Inverted = c.Inverted,
                        LastChanged = c.LastChanged,
                        LastChanger = c.LastChanger,
                        Name = c.Name,
                        Type = c.Type,
                        Value = c.Value
                    }).ToArrayAsync();
            
            if (controls == null)
                return NotFound();
            
            if (controls.Length <= 0)
                return NoContent();

            return Ok(controls);
        }
    }
}