﻿using System.Linq;
using System.Collections.Generic;
using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using RemoteAutomization.Backend.Configuration;
using RemoteAutomization.Backend.Controllers;
using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.Extensions;

#pragma warning disable 1591

namespace RemoteAutomization.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;


            AppConfiguration appConfig = new AppConfiguration();
            configuration.GetSection("AppSettings").Bind(appConfig);
            AppConfiguration = appConfig;
        }

        private AppConfiguration AppConfiguration { get; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string mySqlConnectionstring = Configuration.GetConnectionString("mysql");
            if (string.IsNullOrWhiteSpace(mySqlConnectionstring))
                throw new Exception("No MySql connectionstring was found for the application!");

            if (string.IsNullOrWhiteSpace(AppConfiguration.Security.Jwt.Secret))
                throw new Exception("No valid Jwt secret found for the application!");
            
            UserService.SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppConfiguration.Security.Jwt.Secret));


            services.AddSingleton(
                        new TokenHasher(AppConfiguration.Security.PasswordIterations));
            services.AddSingleton(AppConfiguration);


            services.AddScoped<IUserService, UserService>();

            services.AddDbContext<AppDbContext>(o => 
                o.UseMySql(mySqlConnectionstring));

            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o => {
                o.RequireHttpsMetadata = true;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = UserService.SigningKey,
                    ValidateIssuer = true,
                    ValidIssuer = AppConfiguration.Security.Jwt.Issuer,
                    ValidateAudience = true,
                    ValidAudience = AppConfiguration.Security.Jwt.Audience,
                };
            });

            services.AddCors();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore);
            
            services.AddSwaggerGen(c => {
                c.SwaggerDoc(
                    "v1",
                    new Microsoft.OpenApi.Models.OpenApiInfo { 
                        Title = "Remote Automization API",
                        Version = "v1"
                    }
                );

                c.DescribeAllEnumsAsStrings();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            applicationLifetime.ApplicationStopping.Register(DisconnectWebSockets);
            // env.ConfigureNLog($"nlog.{env.EnvironmentName}.config");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage()
                    .UseDatabaseErrorPage();
            }
            else
            {
                app.ConfigureExceptionHandler(loggerFactory.CreateLogger("GlobalCatcher"));

                app.UseHsts()
                    .UseHttpsRedirection();
            }

            app.UseCors(cors => {
                cors.AllowAnyHeader()
                    .AllowCredentials();

                if (AppConfiguration.Security == null || AppConfiguration.Security.Cors == null)
                    return;


                if (AppConfiguration.Security.Cors.Origins != null && AppConfiguration.Security.Cors.Origins.Length > 0)
                    cors.WithOrigins(AppConfiguration.Security.Cors.Origins);

                if (AppConfiguration.Security.Cors.Methods != null && AppConfiguration.Security.Cors.Methods.Length > 0)
                    cors.WithMethods(AppConfiguration.Security.Cors.Methods);
            });

            app.UseWebSockets(new WebSocketOptions() {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 2048
            });

            app.UseAuthentication();
            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Remote Automization API V1");
                c.RoutePrefix = "doc"; // Set Swagger documentation at doc/.
            });
        }

        private void DisconnectWebSockets()
        {
            foreach (KeyValuePair<uint, IoTWebSocketController> kvp in Controllers.IoTWebSocketController.Sockets)
            {
                kvp.Value.StopService("API Backend shutting down").ConfigureAwait(false);
            }
        }
    }
}
