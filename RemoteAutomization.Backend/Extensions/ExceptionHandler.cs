using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using NLog.Web.AspNetCore;

namespace RemoteAutomization.Backend.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger)
        {
            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
 
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if(contextFeature != null)
                    {
                        APIException exception;
                        if (contextFeature.Error is APIException)
                        {
                            exception = (APIException)contextFeature.Error;
                            context.Response.StatusCode = exception.HttpStatus;
                            logger.LogWarning(exception.InnerException, "APIException ocurred");
                        }
                        else
                        {
                            context.Response.StatusCode = 500;
                            exception = new APIException(500, "Internal server error occured", contextFeature.Error);
                            logger.LogWarning(exception.InnerException, "Unhandled internal exception occured");
                        }
 
                        await context.Response.WriteAsync(exception.ToResponse());
                    }
                });
            });
        }
    }
}