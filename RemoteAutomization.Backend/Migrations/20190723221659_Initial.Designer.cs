﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RemoteAutomization.Backend.EntityFramework;

namespace RemoteAutomization.Backend.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190723221659_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.Control", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<uint>("ControlServiceId");

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("Name");

                    b.Property<byte>("Type");

                    b.Property<uint>("Value");

                    b.HasKey("Id");

                    b.HasIndex("ControlServiceId");

                    b.ToTable("Controls");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.ControlService", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Connected");

                    b.Property<DateTime>("LastSeen");

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ControlServices");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.UserAccount", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<bool>("Enabled");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("NormalizedUsername");

                    b.Property<string>("Password");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.Control", b =>
                {
                    b.HasOne("RemoteAutomization.Backend.EntityFramework.Models.ControlService")
                        .WithMany("Controls")
                        .HasForeignKey("ControlServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
