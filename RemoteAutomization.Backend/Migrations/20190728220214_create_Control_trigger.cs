﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemoteAutomization.Backend.Migrations
{
    public partial class create_Control_trigger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE TRIGGER Control_UpdateLastChange
                    AFTER INSERT
                    ON `ControlEvent`
                    FOR EACH ROW
                BEGIN
                    UPDATE `Controls` SET Value=NEW.Value, LastChanger=NEW.Changer, LastChanged=NEW.TimeStamp WHERE Id=NEW.ControlId AND LastChanged < NEW.TimeStamp;
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP TRIGGER Control_UpdateLastChange;");
        }
    }
}
