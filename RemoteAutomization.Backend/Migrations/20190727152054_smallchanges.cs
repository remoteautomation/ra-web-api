﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemoteAutomization.Backend.Migrations
{
    public partial class smallchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AuthToken",
                table: "ControlServices",
                nullable: true);

            migrationBuilder.AlterColumn<ushort>(
                name: "Value",
                table: "Controls",
                nullable: false,
                oldClrType: typeof(uint));

            migrationBuilder.AddColumn<bool>(
                name: "Inverted",
                table: "Controls",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthToken",
                table: "ControlServices");

            migrationBuilder.DropColumn(
                name: "Inverted",
                table: "Controls");

            migrationBuilder.AlterColumn<uint>(
                name: "Value",
                table: "Controls",
                nullable: false,
                oldClrType: typeof(ushort));
        }
    }
}
