﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RemoteAutomization.Backend.EntityFramework;

namespace RemoteAutomization.Backend.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190728214625_added_ControlEvent")]
    partial class added_ControlEvent
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.Control", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<uint>("ControlServiceId");

                    b.Property<bool>("Inverted");

                    b.Property<DateTime>("LastChanged");

                    b.Property<uint>("LastChanger");

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(64)")
                        .HasMaxLength(64);

                    b.Property<byte>("Type");

                    b.Property<ushort>("Value");

                    b.HasKey("Id");

                    b.HasIndex("ControlServiceId");

                    b.ToTable("Controls");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.ControlEvent", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<uint>("Changer");

                    b.Property<uint>("ControlId");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<ushort>("Value");

                    b.HasKey("Id");

                    b.HasIndex("ControlId");

                    b.ToTable("ControlEvent");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.ControlService", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AuthToken")
                        .IsRequired()
                        .HasColumnType("varchar(60)")
                        .HasMaxLength(60);

                    b.Property<bool>("Connected");

                    b.Property<DateTime>("LastSeen");

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(64)")
                        .HasMaxLength(64);

                    b.HasKey("Id");

                    b.ToTable("ControlServices");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.UserAccount", b =>
                {
                    b.Property<uint>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<bool>("Enabled");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("varchar(32)")
                        .HasMaxLength(32);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("varchar(32)")
                        .HasMaxLength(32);

                    b.Property<DateTime>("Modified");

                    b.Property<uint>("ModifiedBy");

                    b.Property<string>("NormalizedUsername")
                        .IsRequired()
                        .HasColumnType("varchar(24)")
                        .HasMaxLength(24);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("varchar(60)")
                        .HasMaxLength(60);

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("varchar(24)")
                        .HasMaxLength(24);

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.Control", b =>
                {
                    b.HasOne("RemoteAutomization.Backend.EntityFramework.Models.ControlService")
                        .WithMany("Controls")
                        .HasForeignKey("ControlServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("RemoteAutomization.Backend.EntityFramework.Models.ControlEvent", b =>
                {
                    b.HasOne("RemoteAutomization.Backend.EntityFramework.Models.Control")
                        .WithMany("Events")
                        .HasForeignKey("ControlId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
