using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using RemoteAutomization.Backend.Configuration;
using RemoteAutomization.Backend.EntityFramework;
using RemoteAutomization.Backend.EntityFramework.Models;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// UserService repository representation interface.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Authenticate the user using it's username/id and password combination.
        /// </summary>
        /// <param name="identifier">Username or account id</param>
        /// <param name="password">Password</param>
        /// <returns>The corresponding UserAccount instance, null on fail.</returns>
        Task<UserAccount> Authenticate(object identifier, string password);
        /// <summary>
        /// Change the user's password.
        /// </summary>
        /// <param name="accountId">User's account id</param>
        /// <param name="password">The new password</param>
        /// <returns>The success of the password change</returns>
        Task<bool> ChangePassword(uint accountId, string password);
        /// <summary>
        /// Creates a new user with the specified UserAccount details
        /// </summary>
        /// <param name="userAccount">UserAccount with the credentials</param>
        /// <returns>The success of the creation.</returns>
        Task<bool> CreateUser(UserAccount userAccount);
        /// <summary>
        /// Generate a JWT Authentication token.
        /// </summary>
        /// <param name="account">UserAccount to base token from</param>
        /// <returns>The JWT Token</returns>
        string GenerateToken(UserAccount account);
    }

    /// <summary>
    /// The database-interacting UserService repository
    /// </summary>
    public class UserService : IUserService
    {
        /// <summary>
        /// Symmetric signing key for security purposes
        /// </summary>
        internal static SymmetricSecurityKey SigningKey { get; set; }
        /// <summary>
        /// Signing credentials to easily sign using the desired hmac algorithm
        /// </summary>
        internal static SigningCredentials SigningCredentials {
            get {
                if (_signingCredentials == null)
                    _signingCredentials = new SigningCredentials(SigningKey, SecurityAlgorithms.HmacSha384Signature);
                
                return _signingCredentials;
            }
        }
        private static SigningCredentials _signingCredentials;

        private readonly AppConfiguration _appConfig;
        private readonly AppDbContext _dbContext;
        private readonly TokenHasher _tokenHandler;
        private readonly JwtSecurityTokenHandler _jwtTokenHandler;

        /// <summary>
        /// A simple UserAccount wrapper for central, easy access.
        /// </summary>
        /// <param name="appConfig">(Dependency Injection) Application config</param>
        /// <param name="dbContext">(Dependency Injection) Application DbContext</param>
        /// <param name="tokenHasher">(Dependency Injection) Application security-token hasher</param>
        public UserService(AppConfiguration appConfig, AppDbContext dbContext, TokenHasher tokenHasher)
        {
            _appConfig = appConfig;
            _dbContext = dbContext;
            _tokenHandler = tokenHasher;

            _jwtTokenHandler = new JwtSecurityTokenHandler();
        }


        /// <summary>
        /// Authenticate the user and retrieve the corresponding UserAccount model.
        /// </summary>
        /// <param name="identifier">Username or AccountID</param>
        /// <param name="password">Password to validate</param>
        /// <returns>The UserAccount model on susccess, null on fail</returns>
        public async Task<UserAccount> Authenticate(object identifier, string password)
        {
            IQueryable<UserAccount> iqAcc;
            switch (identifier)
            {
                case string t1:
                    iqAcc = _dbContext.Users
                                .Where(c => c.NormalizedUsername == t1.ToLower());
                    break;
                case uint t2:
                    iqAcc = _dbContext.Users
                                .Where(c => c.Id == t2);
                    break;
                default:
                    return null;
            }
            UserAccount acc = await iqAcc.FirstOrDefaultAsync();

            if (acc == null || !acc.Enabled)
                return null;

            if (_tokenHandler.Compare(acc.Password, password))
            {
                acc.Password = null; // Reset/remove password from the class (... yeah)
                return acc;
            }

            return null;
        }

        /// <summary>
        /// Creates the user (without any real validation)
        /// </summary>
        /// <param name="acc"></param>
        /// <returns></returns>
        public async Task<bool> CreateUser(UserAccount acc)
        {
            if (acc == null)
                return false;

            acc.Password = this._tokenHandler.Generate(acc.Password); // Hash password.
            acc.NormalizedUsername = acc.Username.ToLower(); // Update/set the normalized username value
            acc.Created = DateTime.UtcNow;
            acc.Modified = DateTime.UtcNow;

            this._dbContext.Add(acc);

            return (await this._dbContext.SaveChangesAsync() > 0);
        }
        /// <summary>
        /// Change the current password (without validation)
        /// </summary>
        /// <param name="accountId">User account identifier</param>
        /// <param name="password">New password to change to</param>
        /// <returns>The success state</returns>
        public async Task<bool> ChangePassword(uint accountId, string password)
        {
            UserAccount acc = new UserAccount{Id = accountId};
            acc.Password = _tokenHandler.Generate(password);
            _dbContext.Entry(acc).Property("Password").IsModified = true;
            return (await _dbContext.SaveChangesAsync() > 0);
        }

        /// <summary>
        /// Generate a JWT security token.
        /// </summary>
        /// <param name="account">User account for claims</param>
        /// <returns>A JWT token</returns>
        public string GenerateToken(UserAccount account)
        {
            if (account == null || account.Id <= 0)
                return null;

            DateTime dt = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, account.NormalizedUsername),
                    new Claim(ClaimTypes.Sid, account.Id.ToString()),
                    new Claim(ClaimTypes.Role, account.Role)
                }),
                IssuedAt = dt,
                NotBefore = dt,
                Issuer = _appConfig.Security.Jwt.Issuer,
                Audience = _appConfig.Security.Jwt.Audience,
                Expires = dt.AddMinutes(_appConfig.Security.Jwt.Expires),
                SigningCredentials = SigningCredentials
            };
            
            return _jwtTokenHandler.WriteToken(
                _jwtTokenHandler.CreateToken(tokenDescriptor));
        }
    }
}