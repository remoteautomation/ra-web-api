using System;
using Microsoft.AspNetCore.Mvc;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// Generic public error response code
    /// </summary>
    public class APIErrorResponse
    {
        /// <summary>
        /// Error type string
        /// </summary>
        /// <value>The error identifying string</value>
        public string Error { get; set; }
        /// <summary>
        /// Error message
        /// </summary>
        /// <value>The error's public-facing message</value>
        public string Message { get; set; }

        /// <summary>
        /// Creates an error instance with an error-type and a message
        /// </summary>
        /// <param name="error">Error type string</param>
        /// <param name="message">Public-facing message</param>
        public APIErrorResponse(string error, string message = null)
        {
            Error = error;
            Message = message;
        }
    }
}