using System;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// Generic API Exception to be handled by the global error handler.
    /// </summary>
    public class APIException : Exception
    {
        /// <summary>
        /// Http desired response Statuscode
        /// </summary>
        public short HttpStatus { get; private set; } = 500;
        /// <summary>
        /// Public response message
        /// </summary>
        public string ResponseMessage { get; private set; }
        /// <summary>
        /// Timestamp of the exception
        /// </summary>
        /// <value></value>
        public DateTime TimeStamp { get; private set; }

        
        /// <summary>
        /// Generic API Exception to be handled by the global error handler.
        /// </summary>
        /// <param name="httpCode">The error http status-code</param>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception that occurred</param>
        public APIException(short httpCode, string message = null, Exception innerException = null) : base(message, innerException)
        {
            HttpStatus = httpCode;
            ResponseMessage = message;
            TimeStamp = DateTime.UtcNow;
        }

        /// <summary>
        /// Retrieve the public returnable json string
        /// </summary>
        /// <returns>Object's public json stringz</returns>
        public string ToResponse()
        {
            return $"{{\"statusCode\":{ HttpStatus },\"message\":\"{ ResponseMessage }\",\"timestamp\":\"{ TimeStamp.ToString("yyyy-MM-ddTHH:mm:ssZ") }\"}}";
        }
    }
}