using System.Collections.Generic;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// IoT Request type enum
    /// </summary>
    public enum IoTRequestType : ushort
    {
        /// <summary>
        /// Authentication requests
        /// </summary>
        Auth,
        /// <summary>
        /// Command requests
        /// </summary>
        Command,
        /// <summary>
        /// Polling requests, to check for changes
        /// </summary>
        Poll
    }

    /// <summary>
    /// Simple IoTRequests made by the control services
    /// </summary>
    public class IoTRequest
    {
        /// <summary>
        /// The request type
        /// </summary>
        public IoTRequestType Type { get; set; }
        /// <summary>
        /// Service identifier
        /// </summary>
        public uint Service { get; set; }
        /// <summary>
        /// Auth token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Controls send with the request
        /// </summary>
        public List<IoTControl> Controls { get; set; }
    }
}