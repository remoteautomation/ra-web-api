#pragma warning disable 1591

namespace RemoteAutomization.Backend
{
    public class APIResponseStrings
    {
        public const string AuthFailed = "AUTH_FAILED";

        public const string InternalError = "INTERNAL_ERR";
    
        public const string InvalidOldPassword = "INV_OLD_PASSWORD";
        public const string InvalidPassword = "INV_PASSWORD";
        public const string InvalidUsername = "INV_USERNAME";
    }
}