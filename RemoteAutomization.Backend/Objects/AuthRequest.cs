namespace RemoteAutomization.Backend
{
    /// <summary>
    /// AuthRequest body class
    /// </summary>
    public class AuthRequest
    {
        /// <summary>
        /// Account username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Account current/new password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Account old password (for changing)
        /// </summary>
        public string OldPassword { get; set; }
    }
}