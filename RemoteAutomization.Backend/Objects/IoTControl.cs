using System;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// IoT control for plain IoT-based communication
    /// </summary>
    public class IoTControl
    {
        /// <summary>
        /// Control identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Control value
        /// </summary>
        public ushort Value { get; set; }
        /// <summary>
        /// Last control change event date
        /// </summary>
        public DateTime LastChanged { get; set; }
    }
}