using System;

#pragma warning disable 1591

namespace RemoteAutomization.Backend.Configuration
{
    public class AppConfiguration
    {
        public SecurityConfiguration Security { get; set; }
    }

    public class SecurityConfiguration
    {
        public int PasswordIterations { get; set; }

        public CorsConfiguration Cors { get; set; }
        public JwtConfiguration Jwt { get; set; }
    }

    public class CorsConfiguration
    {
        public string[] Origins { get; set; }
        public string[] Methods { get; set; }
    }

    public class JwtConfiguration
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public uint Expires { get; set; }

    }
}