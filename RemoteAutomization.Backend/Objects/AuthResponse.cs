namespace RemoteAutomization.Backend
{
    /// <summary>
    /// Authentication response body
    /// </summary>
    public class AuthResponse
    {
        /// <summary>
        /// JWT authentication token
        /// </summary>
        public string Token { get; set; }
    }
}