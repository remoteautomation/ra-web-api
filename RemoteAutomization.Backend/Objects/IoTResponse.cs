using System.Collections.Generic;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// Response statusses
    /// </summary>
    public enum IoTResponseStatus : byte
    {
        /// <summary>
        /// Failed status
        /// </summary>
        Failed,
        /// <summary>
        /// Success status
        /// </summary>
        Success,
        /// <summary>
        /// Pending status
        /// </summary>
        Pending
    }
    
    /// <summary>
    /// Response types
    /// </summary>
    public enum IoTResponseType : ushort
    {
        /// <summary>
        /// AuthResonse data
        /// </summary>
        AuthResponse,
        /// <summary>
        /// Command response (Control change responses)
        /// </summary>
        Command
    }

    /// <summary>
    /// IoT responses
    /// </summary>
    public class IoTResponse
    {
        /// <summary>
        /// Response type
        /// </summary>
        public IoTResponseType Type { get; set; }
        /// <summary>
        /// Response status value
        /// </summary>
        public IoTResponseStatus Status { get; set; }
        /// <summary>
        /// Response controls
        /// </summary>
        public List<IoTControl> Controls { get; set; }
    }
}