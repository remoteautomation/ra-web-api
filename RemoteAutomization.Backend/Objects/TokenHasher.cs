using Microsoft.Extensions.Options;
using BCrypt.Net;

using RemoteAutomization.Backend.Configuration;

namespace RemoteAutomization.Backend
{
    /// <summary>
    /// A BCrypt tokenhasher wrapper
    /// </summary>
    public class TokenHasher
    {
        private readonly int _iterations;
        
        /// <summary>
        /// Create a hasher instance
        /// </summary>
        /// <param name="iterations">BCrypt iteration count</param>
        public TokenHasher(int iterations)
        {
            _iterations = iterations;
        }

        /// <summary>
        /// Generate a BCrypt password
        /// </summary>
        /// <param name="password">Plain password to hash</param>
        /// <returns></returns>
        public string Generate(string password) =>
            BCrypt.Net.BCrypt.EnhancedHashPassword(password, _iterations, HashType.SHA384);

        /// <summary>
        /// Compare the password against the hash
        /// </summary>
        /// <param name="hash">Hashed password string</param>
        /// <param name="password">Password</param>
        /// <returns>The state of success</returns>
        public bool Compare(string hash, string password) => 
            BCrypt.Net.BCrypt.EnhancedVerify(password, hash, HashType.SHA384); 
    }
}